#!/bin/bash
# Filename:       repte.sh
# Author:         Arroyo Falla, Génesis
# Date:           16/03/2021
# Version:        0.1
# License:        
# Usage:          ./repte.sh
# Description:    Realitzar un script per inserir números a un fitxer, de manera que quedi endreçat. Que els esborri. Que ens digui si el nombre que volem consultar està o no en el fitxer. I per finalitzar que es pugui visualitzar el contingut del fitxer.
#		Fes un menú per demanar les dades per consola
#
#		1. Insereix números a un fitxer.
#		2. Esborra números del fitxer.
#		3. Quin número vols trobar en el fitxer?.
#		4. Visualitza el contingut del fitxer.
#		5. Sortir
#		--- Escull una opció de menú

NUMCORRECTE="^[0-9]+$"

if [ -f fitxer.txt ]; then
        echo "El fitxer existeix."
else
        echo "El fitxer no existeix."
	touch fitxer.txt
fi

echo "
	MENÚ	
  ================
1. Insereix números a un fitxer.
2. Esborra números del fitxer.
3. Quin número vols trobar en el fitxer?
4. Visualitza el contingut del fitxer.
5. Sortir.

 --- Escull una opció de menú:"
read MENU

case $MENU in
		1)
			clear
			echo "Aquests són els números que hi han al fitxer:"
			cat fitxer.txt
			echo "Inserta un número:"
			read NUM
			if ! [[ $NUM =~ $NUMCORRECTE ]]; then
				echo "Això no és un número." >&2
			else
				echo $NUM >> fitxer.txt
			fi
			;;
		2)
			clear
			echo "Aquests són els números que hi han al fitxer:"
			cat fitxer.txt
			echo "Inserta el número que vols esborrar:"
			read NUM
			grep -wv $NUM fitxer.txt > tmp
			mv tmp fitxer.txt
			;;
		3)
			clear
			echo "Inserta un número:"
			read NUM
			grep -w $NUM fitxer.txt
			if [ $? -eq 0 ]; then
				echo "El número $NUM es troba dins del fitxer."
			else
				echo "El número $NUM no es troba dins del fitxer."
			fi
			;;
		4)
			clear
			cat fitxer.txt | sort -n | uniq
			;;
		5)
			exit 0
			;;
			# Sortida.
esac
