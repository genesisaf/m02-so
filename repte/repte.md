- Quina comanda fem servir per netejar la consola?
```
$ clear
```
- Amb bash, com comprovem si un fitxer existeix?
```sh
if [ -f fitxer ]; then
        echo "El fitxer existeix."
else
        echo "El fitxer no existeix."
fi
```
- Amb bash, com creem i/o actualitzem el contingut d’un fitxer?
Creem amb
```
touch
```
i actulitzem amb echo i >> nomdelfitxer
- Amb bash, com eliminem el contingut en un fitxer?
```sh
grep -wv $NUM fitxer.txt > tmp
mv tmp fitxer.txt
```
- Amb bash, com fem un menú d'opcions?
Amb echo y escribim el menú.
- Amb bash, com llegim el que ha introduït per teclat l'usuari?
```
read
```
- Amb bash, quina estructura fem servir quan tenim opcions predefiniedes?
