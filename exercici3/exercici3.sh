#!/bin/bash
# Filename:       exercici3.sh
# Author:         Arroyo Falla, Génesis
# Date:           16/03/2021
# Version:        0.1
# License:        
# Usage:          ./exercici3.sh
# Description:    Modifiqueu l'exercici2, per que mostri el nombre de fitxers donat el directori /var

ls /var | wc -l
