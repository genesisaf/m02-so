#!/bin/bash

systemctl is-active nginx
if [ $? != 0 ]; then
   systemctl start nginx
   echo "Està aturat. S'ha arrancat."
else
   echo "Està encès."
fi
