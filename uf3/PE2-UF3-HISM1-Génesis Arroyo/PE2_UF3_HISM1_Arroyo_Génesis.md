## Autor:          PE2_UF3_HISM1_Arroyo_Géénesis.md
## Date:           28/05/21
## Description:    Prova M03 - UF03 SystemD, Journal, Firewall, Quotes
## Entrega Prova escrita UF3

En finalitzar l'exercici, el fitxer l'heu d'annexar al Moddle, **anomena'l amb el següent format: PE2_UF3_Grup_Cognom_nom.md**
També, recorda pujar-ho al GIT, a la carpeta UF3/prova-escrita/

## Enunciat

Estem encarregats d'administrar el sistema operatiu linux d'un servidor a la nostra empresa. Se'ns demana que fem diverses coses per tal d'obtenir un servidor amb les funcionalitats que es requereixen:

- Cal un servidor web.
- Caldrà establir un tallafocs per tal que no ens accedeixin a ports que no siguin públics.
- Caldrà un accés de ssh per a la gestió remota d'aquest servidor, però volen que només sigui accessible en horari d'oficina (tot i que la màquina estarà en funcionament tot el dia)

1. [2 punts] **Cal que aquesta màquina vostra tingui accés per ssh i que estigui en hora. Per això comprovarem els serveis:**

    - [0,25 punts] Ordre per activar el servei de ssh en iniciar el sistema operatiu:
    ```sh
    [root@a11 ~]# systemctl enable sshd
    ```
    - [0,25 punts] Ordre per arrencar ara el servei de ssh:
    ```sh
    [root@a11 ~]# systemctl start sshd
    ```
    - [0,25 punts] Odre i sortida per veure l'estat del servei ssh:
    ```sh
    [root@a11 ~]# systemctl status sshd
    ● sshd.service - OpenSSH server daemon
     Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor pres>
     Active: active (running) since Fri 2021-05-28 09:20:22 CEST; 50min ago
       Docs: man:sshd(8)
             man:sshd_config(5)
    Main PID: 835 (sshd)
      Tasks: 1 (limit: 9304)
     Memory: 2.0M
        CPU: 10ms
     CGroup: /system.slice/sshd.service
             └─835 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups

    May 28 09:20:22 localhost.localdomain sshd[835]: Server listening on 0.0.0.0 po>
    May 28 09:20:22 localhost.localdomain sshd[835]: Server listening on :: port 22.
    May 28 09:20:21 localhost.localdomain systemd[1]: Starting OpenSSH server daemo>
    May 28 09:20:22 localhost.localdomain systemd[1]: Started OpenSSH server daemon.
    ```
    - [0,25 punts] Ordre per activar el servei d'hora en iniciar el sistema operatiu:
    ```sh
    [root@a11 ~]# systemctl enable servicename.service
    ```
    - [0,25 punts] Ordre per arrencar ara el servei d'hora:
    ```sh
    [root@a11 ~]# systemctl start servicename.service
    ```
    - [0,25 punts] Ordre i sortida per veure l'estat del servei d'hora:
    ```sh
    [root@a11 ~]# systemctl status servicename.service
    ```
    - [0,5 punts] Ordre per veure tots els serveis que es troben arrencats actualment:
    ```sh
    [root@a11 ~]# systemctl list-dependencies
    default.target
    ● ├─accounts-daemon.service
    ● ├─gdm.service
    ● ├─rtkit-daemon.service
    ● ├─switcheroo-control.service
    ● ├─systemd-update-utmp-runlevel.service
    ● ├─udisks2.service
    ● ├─upower.service
    ● └─multi-user.target
    ●   ├─abrt-journal-core.service
    ●   ├─abrt-oops.service
    ●   ├─abrt-vmcore.service
    ●   ├─abrt-xorg.service
    ●   ├─abrtd.service
    ●   ├─atd.service
    ●   ├─auditd.service
    ●   ├─avahi-daemon.service
    ●   ├─chronyd.service
    ●   ├─crond.service
    ●   ├─cups.path
    ●   ├─cups.service
    ●   ├─earlyoom.service
    ●   ├─firewalld.service
    linies 1-23
    ```

2. [2 punts] **Instal.leu ara el servidor web nginx. Aquest servidor ens permetrà tenir un servei en producció:**

    - [0,50 punts] Ordre per instal.lar el servidor web nginx al vostre sistema operatiu:
    ```sh
    [root@a11 ~]# dnf install nginx -y
    ```
    - [0,50 punts] Ordre per activar el servei nginx en iniciar el sistema operatiu:
    ```sh
    [root@a11 ~]# systemctl enable nginx
    Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /usr/lib/systemd/system/nginx.service.
    ```
    - [0,50 punts] Ordre per arrencar ara el servei nginx:
    ```sh
    [root@a11 ~]# systemctl start nginx
    ```
    - [0,50 punts] Ordre i sortida per a veure els últims 10 missatges de log del servei nginx:
    ```sh
    [root@a11 ~]# journalctl -u nginx.service -n 10
    -- Logs begin at Tue 2020-09-15 18:08:47 CEST, end at Fri 2021-05-28 10:56:22 CEST. --
    May 28 10:42:08 a11 systemd[1]: Starting The nginx HTTP and reverse proxy server...
    May 28 10:42:08 a11 nginx[4163]: nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
    May 28 10:42:08 a11 nginx[4163]: nginx: configuration file /etc/nginx/nginx.conf test is successful
    May 28 10:42:08 a11 systemd[1]: Started The nginx HTTP and reverse proxy server.
    ```

3. [2 punts] **Ara cal assegurar el servei ssh i el web com a els únics als que permetem accés:**

   - [0,25 punts] Ordre per activar el servei de tallafocs en iniciar el sistema operatiu:
   ```sh
   [root@a11 ~]# systemctl enable firewalld
   ```
   - [0,25 punts] Ordre per arrencar ara el servei de tallafocs:
   ```sh
   [root@a11 ~]# systemctl start firewalld
   ```
   - [0,25 punts] Ordre i sortida on es vegi els ports que el tallafocs està filtrant actualment i la zona activa:
   ```sh
   [root@a11 ~]# firewall-cmd --get-active-zones
   FedoraWorkstation
        interfaces: enp2s0
   libvirt
        interfaces: virbr0
   [root@a11 ~]# firewall-cmd --list-all
   FedoraWorkstation (active)
    target: default
    icmp-block-inversion: no
    interfaces: enp2s0
    sources: 
    services: dhcpv6-client samba-client ssh
    ports: 1025-65535/udp 1025-65535/tcp
    protocols: 
    masquerade: no
    forward-ports: 
    source-ports: 
    icmp-blocks: 
    rich rules: 
   ```
   - [0,5 punts] Ordre(s) per tal d'aconseguir que només siguin accessibles els ports 22 i 80 del servidor a partir de la informació mostrada en l'ordre anterior:
   ```sh
   [root@a11 ~]# firewall-cmd --zone=FedoraWorkstation --add-port=22/tcp
   success
   [root@a11 ~]# firewall-cmd --zone=FedoraWorkstation --add-port=80/tcp
   success
   ```
   - [0,5 punts] Ordre i sortida on es vegi els ports que el tallafocs està filtrant actualment i la zona activa:
   ```sh

   ```
   - [0,25 punts] Feu que aquesta configuració sigui permanent. Indiqueu la/les ordre(s) per a fer-ho:
   ```sh
   [root@a11 ~]# firewall-cmd --list-all
   FedoraWorkstation (active)
    target: default
    icmp-block-inversion: no
    interfaces: enp2s0
    sources: 
    services: dhcpv6-client samba-client ssh
    ports: 1025-65535/udp 1025-65535/tcp 80/tcp 22/tcp
    protocols: 
    masquerade: no
    forward-ports: 
    source-ports: 
    icmp-blocks: 
    rich rules: 
   ```

4. [2 punts] **Feu ara dos scripts que comprovin l'estat del servei nginx. Un l'haurà d'arrencar si no ho està i l'altre l'haurà d'aturar si està arrencat**

    - [0,5 punts] Script que comprova si està arrencat i l'atura
    **nginx1.sh**
    ```sh
    #!/bin/bash
    
    systemctl is-active nginx
    if [ $? == 0 ]; then
        systemctl stop nginx
        echo "Està encès. S'ha aturat."
    else
        echo "Està aturat."
    fi
    ```
    - [0,5 punts] Script que comprova si està aturat i l'arrenca
    **nginx2.sh**
    ```sh
    #!/bin/bash
    
    systemctl is-active nginx
    if [ $? != 0 ]; then
        systemctl start nginx
        echo "Està aturat. S'ha arrancat."
    else
        echo "Està encès."
    fi
    ```
    - [0,5 punts] Què hauríem de canviar al nostre script per tal que enviés un missatge de prioritat 'info' al journal amb informació que ens digui si l'hem arrencat o si l'hem parat?
    ```
    hauriem de posar al script 'journalctl-cat -p info'
    ```
    - [0,5 punts] Com podem veure els missatges de prioritat 'info' del log?
    ```sh
    [root@a11 ~]# journalctl -p info -b
    -- Logs begin at Tue 2020-09-15 18:08:47 CEST, end at Fri 2021-05-28 11:16:01 CEST. --
    May 28 09:19:35 localhost kernel: microcode: microcode updated early to revision 0x28, date = 2019-11-12
    May 28 09:19:35 localhost kernel: Linux version 5.8.8-200.fc32.x86_64 (mockbuild@bkernel01.iad2.fedoraproj>
    May 28 09:19:35 localhost kernel: Command line: BOOT_IMAGE=/boot/vmlinuz-0-rescue-8d3cbbb6bced4752ab4d018e>
    May 28 09:19:35 localhost kernel: x86/fpu: Supporting XSAVE feature 0x001: 'x87 floating point registers'
    May 28 09:19:35 localhost kernel: x86/fpu: Supporting XSAVE feature 0x002: 'SSE registers'
    May 28 09:19:35 localhost kernel: x86/fpu: Enabled xstate features 0x3, context size is 576 bytes, using '>
    May 28 09:19:35 localhost kernel: BIOS-provided physical RAM map:
    May 28 09:19:35 localhost kernel: BIOS-e820: [mem 0x0000000000000000-0x000000000009d7ff] usable
    May 28 09:19:35 localhost kernel: BIOS-e820: [mem 0x000000000009d800-0x000000000009ffff] reserved
    ```

5. [2 punts]  **Una mica sobre quotes:** 
   - [0,25 punts] Per a que serveixen les quotes de disc?
   ```
   Es pot restringir  l’espai en disc mitjançant l’implementació de quotes de disc que alertin l’administrador del sistema abans que un usuari consumeixi massa espai en disc o una partició s’omplí.
   ```
   - [0,50 punts] creem un disc virtual de 80Mb anomenat examen a la carpeta /home/
   ```
   [root@a11 ~]# dd if=/dev/zero of=/home/guest/examen bs=80k count=1000
   ```
   - [0,25 punts] Al disc virtual que acabem de crear li donem format ext4
   ```
   [root@a11 ~]# mkfs.ext4 /home/guest/examen
   ```
   - [0,50 punts] Montem l'arxiu examen a la carpeta /mnt/usuari
   ```
   [root@a11 ~]# mount -o loop /home/guest/examen /mnt/usuari
   ```
   - [0,25 punts] Quin fitxer hem de modificar, per tal de que es munti el disc automàticament en reiniciar el sistema. 
   ```
   el fitxer fstab
   ```
   - [0,25 punts] Que és un inode?
   ```
   És una estructura de dades propia dels sistemes d'arxius als sistemes operatius tipus UNIX como éés el cas de Linux.
   ```
