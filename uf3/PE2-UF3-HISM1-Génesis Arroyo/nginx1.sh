#!/bin/bash

systemctl is-active nginx
if [ $? == 0 ]; then
   systemctl stop nginx
   echo "Està encès. S'ha aturat."
else
   echo "Està aturat."
fi
