#!/bin/bash
# Filename:       exercici2.sh
# Author:         Arroyo Falla, Génesis
# Date:           16/03/2021
# Version:        0.1
# License:        
# Usage:          ./exercici2.sh
# Description:    Mostra el nombre de fitxers al directori de treball actual.

ls | wc -l
