- Quina ordre fem servir per mostrar els fitxers d'un directori?
```
ls
```
- Quina ordre fem servir, per comptar el número de fitxers d'un directori?
```
ls | wc -l
```
Si volem comptar fitxers i subdirectoris:
```
find . | wc -
```
