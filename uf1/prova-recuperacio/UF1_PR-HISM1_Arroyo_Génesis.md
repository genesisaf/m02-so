## Autor:          UF1_PR-HISM1_Arroyo_Génesis.md
## Date:           01/06/21
## Description:    Prova Recuperació M02 - UF01 Sistemes Operatius. Màquines virtuals.


### Entrega 
### 1. En finalitzar l'exercici, el fitxer l'heu d'annexar al Moddle, 
###    **Recorda renombrar el fitxer amb el següent format: UF1_PR-Grup_Cognom_alumne.md**
### 2. Annexa també, el Fitxer kdeneon.xml de l'apartat de màquines virtuals.
### 3. També, recorda pujar tots els fitxers al GIT, a la carpeta UF1/prova-recuperacio

## Conceptes de Sistemes Operatius [3 PUNTS]

1. Què és el *kernel* o nucli d'un sistema operatiu?

   Resposta: És un software que forma una part fundamental del sistema operatiu, i és la part que s'executa en mode privilegiat. Coordina l'ús del processador, que també és el nucli del maquinari, el qual realitza molts processos alhora; el kernel prioritza aquests processos assignant-los un temps determinat a cadascun. Així s'evita que el procés d'una aplicació consumeixi el processador que necessiten altres aplicacions.

2. Què és i quines funcions realitza el planificador de cpu?

   Resposta: És un component molt important dels sistemes operatius. És multitasca i multiprocés. Reparteix el temps disponible d'un microprocessador entre tots els processos que estan disponibles per a la seva execució.
   Funcions:
- garanteix que cada procés obtingui la proporció justa de la CPU
- manté ocupada la CPU el cent per cent del temps
- minimitza el temps de resposta per als usuaris interactius
- maximitza el nombre de tasques processades per hora

3. Què és i quines funcions realitza el gestor de memòria i quina és la funció de la memòria *swap*?

   Resposta:
   Prèn decisions sobre aspectes diversos, com:
   - quant espai es dedica a cada procés
   - de quina manera se li assigna
   - en quin lloc s’ubica
   - durant quant de temps roman en memòria
   - com actúúa si no hi ha prou espai
   - com es protegeix davant accessos incorrectes

   Funció memòria swap:
   És la memòria secundària. Es troba en disc i es fa servir quan s'omple la RAM. És molt més lenta. És on es reserva per a moure-hi dades d'aplicacions emmagatzemades en RAM quan aquestes aplicacions ja no s'usen tant.

4. Indica tres fonts d'informació per a transaccions:

   Resposta 1: **Microsoft SQL Server**         Resposta 2: **MongoDB**         Resposta 3: **Oracle Database**

5. Indica tres sortides d'informació per a transaccions:

   Resposta 1: **Microsoft SQL Server**         Resposta 2: **MongoDB**         Resposta 3: **Oracle Database**

6. Què és i quines funcions realitza el planificador de cpu?
   
   Resposta: És un component molt important dels sistemes operatius. És multitasca i multiprocés. Reparteix el temps disponible d'un microprocessador entre tots els processos que estan disponibles per a la seva execució.
   Funcions:
- Garanteix que cada procés obtingui la proporció justa de la CPU.
- Manté ocupada la CPU el cent per cent del temps.
- Minimitza el temps de resposta per als usuaris interactius.
- Maximitza el nombre de tasques processades per hora.

## Comandes [3 PUNTS]

1. Quina ordre ens permet canviar els permisos del fitxer mostrat per tal que només el grup al que pertany pugui executar-lo?
   3826509     4 -rw-r--r--.  1 darta darta      874  2 oct 16:19 install.sh

   Resposta:
   ```
   chmod g=rx install.sh
   ```
   **Es veuria així: -rw-r-xr--**

2. Quins permisos tindrà el grup d'usuaris sobre un fitxer al que apliquem els permisos de fitxer 640?

   Resposta: **-rw-r-----**
   ```
   L'usuari pot llegir i escriure, el grup pot llegir i altres no poden fer res
   ```

3. Com podem canviar el propietari del fitxer install.sh anterior per tal que ara sigui l'usuari 'pepito'?

   Resposta:
   ```
   chown pepito install.sh
   ```

4. Amb quines comandes podem mostrar el contingut d'un fitxer (sense editar-lo)? Indiqueu dues comandes!

   Resposta 1: **cat**            Resposta 2: **tail**

5. Amb quina comanda podem veure els permisos i propietaris d'un arxiu o directori? 

   Resposta: **ls -lisa**

6. Quina comanda ens retorna la ruta del directori on ens trobem?. I per canviar de directori?
   Resposta 1: **pwd**                  Resposta 2: **cd**

## Processos i màquines virtuals [4 punts]

Creareu una màquina virtual KVM a partir de la distribució KDE Neon. Seguir els passos indicats:

1. Descarregueu des de la línia d'ordres l'imatge de KDE Neon en segon pla (background). Indiqueu les ordres que heu introduït per a fer-ho.

   Resposta:
   ```sh
   [guest@a11 ~]$ wget https://files.kde.org/neon/images/user/20210527-0944/neon-user-20210527-0944.iso
   ```

2. Creeu dos discs per a fer-hi la instal.lació i indiqueu les ordres que heu utilitzat:

- Disc 1:
  - Nom: kdeneon.qcow2
  - Tamany: 10G
  - Format: qcow incremental

- Disc 2:
  - Nom: data.qcow2
  - Tamany: 10M
  - Format: qcow incremental

  Resposta:
  - **disk 1: qemu-img create -f qcow2 kdeneon.qcow2 10G**
  - **disk 2: qemu-img create -f qcow2 data.qcow2 10M**

3. Creeu ara la màquina amb les dades següents i, una vegada creada, mentre s'estigui instal.lant, volqueu-ne la definició (format xml) en un fitxer anomenat kdeneon.xml que també haureu d'entregar.

```sh
[root@a11 ~]# virsh dumpxml kde_neon
[root@a11 ~]# virsh dumpxml kde_neon > kdeneon.xml
```

- Nom: kde neon
- Memòria: 800M
- Nombre de fils de processador: 2
- Instal.lació: Des de la ISO descarregada de KDE Neon
- Disc principal on instal.lar-ho: kdeneon.qcow2 (creat abans). Poseu-lo amb bus *virtio*
- Disc secundari: data.qcow2. Poseu-o ambm bus *sata*
- Interfícies de xarxa: Dues, una conectada directament a l'amfitrió i l'altre amb NAT.

4. Durant la instal.lació indiqueu també:
   - Ordre i sortida per pantalla per veure el procés qemu que s'està executant:
   - Sobre l'ordre anterior indica quin és el PID del procés mostrat:
   - Ordre per posar la màquina en pausa:
   ```
   virsh destroy kde_neon
   ```
   or
   ```
   virsh suspend kde_neon
   ```
   - Ordre per reiniciar la màquina virtual pausada anteriorment:
   ```
   virsh start kde_neon
   ```
