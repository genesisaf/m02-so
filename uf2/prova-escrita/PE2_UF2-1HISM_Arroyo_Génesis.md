## Autor:          PE2_UF2_1HISM_Arroyo_Génesis.md
## Date:           13/04/21
## Description:    Prova M02 - UF02 Linux 1  
## Entrega Prova escrita:

1. **( 1 punt ) Copia aquest fitxer en el teu repositori remot ( GitLab, GitHub ), a la carpeta /m-02/uf2/prova-escrita/** 
**Escriu aquí el link.** 
https://gitlab.com/genesisaf/m02-so/-/tree/master/uf2/prova-escrita

2. **(1 punt ) Escriu un script que ens dongui informació sobre la caducitat i validesa dels usuaris del Sistema.**
```sh
#!/bin/bash
# Filename:       caducitat.sh
# Author:         Arroyo Falla, Génesis
# Date:           13/04/2021
# Version:        0.1
# License:        
# Usage:          ./caducitat.sh
# Description:    Escriu un script que ens dongui informació sobre la caducitat i validesa dels usuaris del Sistema.

chage -l nomusuari
```

**Si l'ordre retorna un estat de sortida 0, informeu que "l'ordre s'ha realitzat correctament" i sortiu amb un estat de sortida 0.** 
**Si l'ordre retorna un estat de sortida diferent de zero, informeu "Comanda fallida" i sortiu amb un estat de sortida 1.** 

3. **( 1 punt ) Quin cron farem servir per tal que l'script anterior es faci a les 23:45 cada diumenge?** 
```sh
45 23 * * 7
```

4. **(1 punt ) Feu un script que comprovi si existeix el fitxer "/tmp/prova". Si no existeix el crea. Feu que informi a l'usuari del que ha passat.**
```sh
#!/bin/bash
# Filename:       comprova.sh
# Author:         Arroyo Falla, Génesis
# Date:           13/04/2021
# Version:        0.1
# License:        
# Usage:          ./comprova.sh
# Description:    Feu un script que comprovi si existeix el fitxer "/tmp/prova". Si no existeix el crea. Feu que informi a l'usuari del que ha passat.

DIRECTORI="/tmp/prova"

if [ -e $DIRECTORI ]; then
	echo "El fitxer $DIRECTORI existeix."
else
	mkdir /tmp/prova
	echo "El fitxer $DIRECTORI no existeix."
	sleep 2
	echo "El fitxer $DIRECTORI s'ha creat."
fi
```

5. **(1 punt ) Que estem fent en la següent comanda? grep -w 8 fitxer**
```
Selecciona només aquelles línies que contenen coincidències que formen paraules senceres.
```

6. **(1 punt ) Com comprovem que la anterior ordre s'hagi executat amb èxit?.**


7. **(1 punt ) Crea un fitxer .txt de nom la data i hores actuals a /tmp**
```sh
#!/bin/bash

DIA=`date +"%d/%m/%Y"`
HORA=`date +"%H:%M"`
FILE="$DIA$HORA.txt"

touch $FILE /tmp
```

8. **(1 punt ) Fer un script que rep tres arguments i valida que siguin exactament 3.**


9. **(1 punt ) Que fa la següent comanda? cat file1.txt file2.txt file3.txt | sort > file4.txt**
```
Copia el que hi ha als fitxers file1.txt, file2.txt i file3.txt en el fitxer file4.txt
```

10. **(1 punt ) Si tenim en un fitxer el següent contingut:**

```
one	two	three	four	five
alpha	beta	gamma	delta	epsilon
gener   febrer  març    abril
```

**Quina ordre hem d'executar per obtenir**
```
two
beta
febrer
```

Hem d'executar l'ordre:
```sh
cut -f 2 nomarxiu
```
